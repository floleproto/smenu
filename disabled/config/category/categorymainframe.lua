SMenu = SMenu or {}
SMenu.frame.config = SMenu.frame.config or {}
SMenu.frame.config.category = SMenu.frame.config.category or {}

function SMenu.frame.config.category.Show()

	SMenu.frame.config.category.categorymainFrame = vgui.Create("SFrame")
	SMenu.frame.config.category.categorymainFrame:MakePopup()
	SMenu.frame.config.category.categorymainFrame:SetTitle(SMenu.Language["Parameters"])
	SMenu.frame.config.category.categorymainFrame:SetSize(ScrW()*0.8, ScrH()*0.8)
	SMenu.frame.config.category.categorymainFrame:Center()

	SMenu.frame.config.categoryoption.Show()
	SMenu.frame.config.categorylist.Show()
	
end
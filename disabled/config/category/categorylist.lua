SMenu = SMenu or {}
SMenu.frame.config.categorylist = SMenu.frame.config.categorylist or {}
SMenu.frame.config.categoryconfig = SMenu.frame.config.categoryconfig or {}


function SMenu.frame.config.categorylist.Reload()
	SMenu.frame.config.categoriesList_scrollpanel:Clear()
	SMenu.frame.config.categoryconfig.Reload()
	local pw, ph = SMenu.frame.config.categoriesList_scrollpanel:GetSize()
	for _, v in pairs(SMenu.frame.config.categoryconfig.categories) do
		local button = SMenu.frame.config.categoriesList_scrollpanel:Add("SMenuButton")
		button:SetSize(pw, 50)
		button.categoryName = v.name
		button:SetText(v.name)
		button:Dock(TOP)
		function button:DoClick()
			SMenu.frame.config.categoryoption.SetCategoryActive(button.categoryName)
		end
	end
end

function SMenu.frame.config.categorylist.Show()

	local mfx, mfy = SMenu.frame.config.category.categorymainFrame:GetSize()
	
	SMenu.frame.config.categoriesList = vgui.Create("DPanel", SMenu.frame.config.category.categorymainFrame)
	SMenu.frame.config.categoriesList:SetSize(mfx * 0.3, mfy - 50)
	SMenu.frame.config.categoriesList:SetPos(0, 50)
	SMenu.frame.config.categoriesList.Paint = function(s, w, h)
		draw.RoundedBox(0,0, 0, w, h, SMenu.Color.GetColor("secondary"))
	end


	local clx, cly = SMenu.frame.config.categoriesList:GetSize()

	SMenu.frame.config.categoriesList_scrollpanel = vgui.Create("SScrollPanel", SMenu.frame.config.categoriesList)
	SMenu.frame.config.categoriesList_scrollpanel:SetSize(clx, cly * 0.86)

	SMenu.frame.config.categoriesList_addbutton = vgui.Create("SMenuButton", SMenu.frame.config.categoriesList)
	SMenu.frame.config.categoriesList_addbutton:SetPos(621, cly - 75)
	SMenu.frame.config.categoriesList_addbutton.HoverColor = SMenu.Color.GetColor("green")
	SMenu.frame.config.categoriesList_addbutton:SetSize(300, 50)
	SMenu.frame.config.categoriesList_addbutton:CenterHorizontal()
	SMenu.frame.config.categoriesList_addbutton:SetText(SMenu.Language["Add"])
	function SMenu.frame.config.categoriesList_addbutton:DoClick()
		SMenu.frame.config.category.categorymainFrame:SetMouseInputEnabled(false)
		SMenu.frame.config.category.categorymainFrame:SetKeyboardInputEnabled(false)
		SMenu.frame.popup.AskText(SMenu.Language["CategoryQuestion"], function(res, pnl)
			if(IsValid(SMenu.frame.config.category.categorymainFrame)) then
				if(SMenu.frame.config.categoryconfig.GetCategory(name) != nil) then
					SMenu.frame.popup.Info(SMenu.Language["AlreadyExist"])
					return
				end
				SMenu.frame.config.categoryconfig.AddCategory(res)
				SMenu.frame.config.categorylist.Reload()
			else
				pnl:Close()
				SMenu.frame.popup.Info(SMenu.Language["PanelDoesntExist"])
			end
		end, function ()
			SMenu.frame.config.category.categorymainFrame:SetMouseInputEnabled(true)
			SMenu.frame.config.category.categorymainFrame:SetKeyboardInputEnabled(true)
		end)
	end

	SMenu.frame.config.categorylist.Reload()

end
SMenu = SMenu or {}


local function LoadFile(path, type)

	local fi, fo = file.Find(path .. "*", "LUA")

	for _, v in pairs(fi) do

		if(type == "sv") then

			include(path .. v)
			print("[SV] " .. path .. v .. " loaded.")

		elseif(type == "cl") then

			AddCSLuaFile(path .. v)
			if CLIENT then
				include(path .. v)
			end
			print("[CL] " .. path .. v .. " loaded.")

		elseif(type == "sh") then

			include(path .. v)
			AddCSLuaFile(path .. v)
			print("[SH] " .. path .. v .. " loaded.")

		end

	end

	for _,v in pairs(fo) do

		LoadFile(path .. v .. "/", type)

	end

end

print("---------   SMenu   -----------")

LoadFile("smenu/shared/", "sh")
LoadFile("smenu/server/", "sv")
LoadFile("smenu/client/", "cl")

print("--------- Init ended -----------")

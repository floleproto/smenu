SMenu = SMenu or {}
SMenu.frame = SMenu.frame or {}
SMenu.frame.dontClose = false

hook.Add( "OnSpawnMenuOpen", "SMenu:OnSpawnMenuOpen:Open", function()
	if(GetConVar("open_oldmenu"):GetInt() == 1 && table.HasValue(SMenu.Config.OldMenu, LocalPlayer():GetUserGroup())) then
		RunConsoleCommand( "open_oldmenu", "0" )
		return
	end
	achievements.SpawnMenuOpen()
	SMenu.frameFunc.OpenMenu()
	return false
end)

hook.Add( "OnSpawnMenuClose", "SMenu:OnSpawnMenuClose:Close", function()
	if IsValid(SMenu.frame.main_panel) && !SMenu.frame.dontClose then
		SMenu.frameFunc.CloseMenu()
	end
end)

hook.Add( "OnTextEntryGetFocus", "SMenu:OnTextEntryGetFocus:DontClose", function(panel)
	if(IsValid(SMenu.frame.main_panel) && IsValid(SMenu.frame.tool_panel)) then
		SMenu.frame.dontClose = true
	end
end)

hook.Add( "Initialize", "SMenu:Initialize:Close", function()
	SMenu.RegisterVehicles()
	SMenu.RegisterWeapons()
	SMenu.RegisterEnts()
end)
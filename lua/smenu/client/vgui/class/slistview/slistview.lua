local PANEL = {}

function PANEL:Init()

	self:SetHeaderHeight(25)
	
	self.VBar.Paint = function( s, w, h )
	end
	self.VBar.btnUp.Paint = function( s, w, h )
	end
	self.VBar.btnDown.Paint = function( s, w, h )
	end
	self.VBar.btnGrip.ActualColor = SMenu.Color.GetColor("black")
	self.VBar.btnGrip.Paint = function( s, w, h )
		if s.Depressed then
			s.ActualColor = SMenu.utils.LerpColor(16 * RealFrameTime(), s.ActualColor, SMenu.Color.GetColor("primary"))
			draw.RoundedBox(0, 0, 0, w, h, s.ActualColor)
			return
		end
		
		if s:IsHovered() then
			s.ActualColor = SMenu.utils.LerpColor(8 * RealFrameTime(), s.ActualColor, SMenu.Color.GetColor("accent"))
			draw.RoundedBox(0, 0, 0, w, h, s.ActualColor)
		else
			s.ActualColor = SMenu.utils.LerpColor(8 * RealFrameTime(), s.ActualColor, SMenu.Color.GetColor("black"))
			draw.RoundedBox(0, 0, 0, w, h, s.ActualColor)
		end
	end

	self:SetDataHeight(30)
end

function PANEL:AddColumn( strName, iPosition )

	local pColumn = nil
	
	if ( self.m_bSortable ) then
		pColumn = vgui.Create( "SListView_Column", self )
	else
		pColumn = vgui.Create( "SListView_ColumnPlain", self )
	end

	pColumn:SetName( strName )
	pColumn:SetZPos( 10 )


	function pColumn:Paint(w, h)
		draw.RoundedBox(0, 0, 0, w, h, SMenu.Color.GetColor("black"))
	end

	if ( iPosition ) then

		table.insert( self.Columns, iPosition, pColumn )

		for i = 1, #self.Columns do
			self.Columns[ i ]:SetColumnID( i )
		end

	else

		local ID = table.insert( self.Columns, pColumn )
		pColumn:SetColumnID( ID )

	end

	self:InvalidateLayout()

	return pColumn

end

function PANEL:AddLine( ... )

	self:SetDirty( true )
	self:InvalidateLayout()

	local Line = vgui.Create( "SListView_Line", self.pnlCanvas )
	local ID = table.insert( self.Lines, Line )

	Line:SetListView( self )
	Line:SetID( ID )

	for k, v in pairs( self.Columns ) do
		Line:SetColumnText( k, "" )
	end

	for k, v in pairs( {...} ) do
		Line:SetColumnText( k, v )
	end

	local SortID = table.insert( self.Sorted, Line )

	if ( SortID % 2 == 1 ) then
		Line:SetAltLine( true )
	end

	return Line

end

function PANEL:Paint(w, h)
	draw.RoundedBox(0, 0, 0, w, h, SMenu.Color.GetColor("secondary"))
end

vgui.Register("SListView", PANEL, "DListView")
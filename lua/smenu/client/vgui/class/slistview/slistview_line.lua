local PANEL = {}

function PANEL:Init()

	self.ActualColor = SMenu.Color.GetColor("black")

end

function PANEL:Paint(w, h)
	if self:IsLineSelected() then
		self.ActualColor = SMenu.utils.LerpColor(16 * RealFrameTime(), self.ActualColor, SMenu.Color.GetColor("accent"))
		draw.RoundedBox(0, 0, 0, w, h, self.ActualColor)
		return
	end
	
	if self:IsHovered() then
		self.ActualColor = SMenu.utils.LerpColor(8 * RealFrameTime(), self.ActualColor, SMenu.Color.GetColor("accent"))
		draw.RoundedBox(0, 0, 0, w, h, self.ActualColor)
	else
		self.ActualColor = SMenu.utils.LerpColor(8 * RealFrameTime(), self.ActualColor, SMenu.Color.GetColor("secondary"))
		draw.RoundedBox(0, 0, 0, w, h, self.ActualColor)
	end
end

function PANEL:SetColumnText( i, strText )

	if ( type( strText ) == "Panel" ) then

		if ( IsValid( self.Columns[ i ] ) ) then self.Columns[ i ]:Remove() end

		strText:SetParent( self )
		self.Columns[ i ] = strText
		self.Columns[ i ].Value = strText
		return

	end

	if ( !IsValid( self.Columns[ i ] ) ) then

		self.Columns[ i ] = vgui.Create( "SListViewLabel", self )
		self.Columns[ i ]:SetMouseInputEnabled( false )

	end

	self.Columns[ i ]:SetText( tostring( strText ) )
	self.Columns[ i ].Value = strText
	return self.Columns[ i ]

end

vgui.Register("SListViewLine", PANEL, "DListViewLine")
vgui.Register("SListView_Line", PANEL, "DListView_Line")

local PANEL = {}

function PANEL:Init()

	self:SetFont("SMenu:Text")

end

function PANEL:UpdateColours( skin )

	return self:SetTextStyleColor( SMenu.Color.GetColor("white") )

end

vgui.Register("SListViewLabel", PANEL, "DListViewLabel")

local PANEL = {

	Init = function( self )

		self:SetContentAlignment( 4 )
		self:SetTextInset( 5, 0 )
		self.Color = SMenu.Color.GetColor("black")
		self.ActualColor = SMenu.Color.GetColor("black")

	end,

	DoClick = function( self )

		self:GetParent():Toggle()

	end,

	Paint = function( self, w, h )

		if self.Depressed then
			self.ActualColor = SMenu.utils.LerpColor(16 * RealFrameTime(), self.ActualColor, SMenu.Color.GetColor("black"))
			draw.RoundedBox(0, 0, 0, w, h, self.ActualColor)
			return
		end
	
		if self:IsHovered() then
			self.ActualColor = SMenu.utils.LerpColor(8 * RealFrameTime(), self.ActualColor, self.HoverColor)
			draw.RoundedBox(0, 0, 0, w, h, self.ActualColor)
		else 
			self.ActualColor = SMenu.utils.LerpColor(8 * RealFrameTime(), self.ActualColor, self.Color)
			draw.RoundedBox(0, 0, 0, w, h, self.ActualColor)
		end
		if self:GetParent():GetExpanded() then
			draw.RoundedBox(0, 0, 0, w, h, self.HoverColor)
		end

	end,

	GenerateExample = function()
	end

}

derma.DefineControl( "SCategoryHeader", "Category Header", PANEL, "SMenuButton" )
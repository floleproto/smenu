local PANEL = {}

function PANEL:Init()

	self.VBar.Paint = function( s, w, h )
	end
	self.VBar.btnUp.Paint = function( s, w, h )
	end
	self.VBar.btnDown.Paint = function( s, w, h )
	end
	self.VBar.btnGrip.ActualColor = SMenu.Color.GetColor("black")
	self.VBar.btnGrip.Paint = function( s, w, h )
		if s.Depressed then
			s.ActualColor = SMenu.utils.LerpColor(16 * RealFrameTime(), s.ActualColor, SMenu.Color.GetColor("primary"))
			draw.RoundedBox(0, 0, 0, w, h, s.ActualColor)
			return
		end
		
		if s:IsHovered() then
			s.ActualColor = SMenu.utils.LerpColor(8 * RealFrameTime(), s.ActualColor, SMenu.Color.GetColor("accent"))
			draw.RoundedBox(0, 0, 0, w, h, s.ActualColor)
		else
			s.ActualColor = SMenu.utils.LerpColor(8 * RealFrameTime(), s.ActualColor, SMenu.Color.GetColor("black"))
			draw.RoundedBox(0, 0, 0, w, h, s.ActualColor)
		end
	end
end

function PANEL:PerformLayout()

	local Tall = self.pnlCanvas:GetTall()
	local Wide = self:GetWide()
	local YPos = 0

	self:Rebuild()

	self.VBar:SetUp( self:GetTall(), self.pnlCanvas:GetTall() + 25 )
	YPos = self.VBar:GetOffset()

	if ( self.VBar.Enabled ) then Wide = Wide - self.VBar:GetWide() end

	self.pnlCanvas:SetPos( 0, YPos )
	self.pnlCanvas:SetWide( Wide )

	self:Rebuild()

	if ( Tall != self.pnlCanvas:GetTall() ) then
		self.VBar:SetScroll( self.VBar:GetScroll() ) -- Make sure we are not too far down!
	end

end

function PANEL:Paint(w,h)
	
end

vgui.Register("SScrollPanel", PANEL, "DScrollPanel")
local PANEL = {}

function PANEL:Init()
	self.VBar.Paint = function( s, w, h )
	end
	self.VBar.btnUp.Paint = function( s, w, h )
	end
	self.VBar.btnDown.Paint = function( s, w, h )
	end
	self.VBar.btnGrip.ActualColor = SMenu.Color.GetColor("black")
	self.VBar.btnGrip.Paint = function( s, w, h )
		if s.Depressed then
			s.ActualColor = SMenu.utils.LerpColor(16 * RealFrameTime(), s.ActualColor, SMenu.Color.GetColor("primary"))
			draw.RoundedBox(0, 0, 0, w, h, s.ActualColor)
			return
		end
		
		if s:IsHovered() then
			s.ActualColor = SMenu.utils.LerpColor(8 * RealFrameTime(), s.ActualColor, SMenu.Color.GetColor("accent"))
			draw.RoundedBox(0, 0, 0, w, h, s.ActualColor)
		else
			s.ActualColor = SMenu.utils.LerpColor(8 * RealFrameTime(), s.ActualColor, SMenu.Color.GetColor("black"))
			draw.RoundedBox(0, 0, 0, w, h, s.ActualColor)
		end
	end
end

function PANEL:Paint(w, h)
	draw.RoundedBox(0, 0, 0, w, h, SMenu.Color.GetColor("secondary"))
end

vgui.Register("SIconBrowser", PANEL, "DIconBrowser")
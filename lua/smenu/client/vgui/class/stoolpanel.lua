local PANEL = {}

AccessorFunc( PANEL, "m_TabID", "TabID" )

function PANEL:Init()

	self.HorizontalDivider = vgui.Create( "DHorizontalDivider", self )
	self.HorizontalDivider:Dock( FILL )
	self.HorizontalDivider:SetLeftWidth( 256 )
	self.HorizontalDivider:SetLeftMin( 130 )
	self.HorizontalDivider:SetRightMin( 256 )
	self.HorizontalDivider:SetDividerWidth( 8 )

	local leftContainer = vgui.Create( "Panel", self.HorizontalDivider )
	function leftContainer:Paint(width, height)
		
	end

	self.List = vgui.Create( "SCategoryList", leftContainer )
	self.List:Dock( FILL )

	self.HorizontalDivider:SetLeft( leftContainer )

	local rightPanel = vgui.Create("DPanel")
	function rightPanel:Paint(width, height)
		
	end

	self.Content = vgui.Create( "SCategoryList", self.HorizontalDivider )
	self.HorizontalDivider:DockMargin(0, 40, 0, 0)
	self.HorizontalDivider:SetRight( self.Content )

end

function PANEL:LoadToolsFromTable( inTable )

	local inTable = table.Copy( inTable )

	for k, v in pairs( inTable ) do

		if ( istable( v ) ) then

			local Name = v.ItemName
			local Label = v.Text
			v.ItemName = nil
			v.Text = nil

			self:AddCategory( Name, Label, v )

		end

	end

end

function PANEL:AddCategory( name, lbl, tItems )
	local Category = self.List:Add(lbl)

	local bAlt = true

	local tools = {}
	for k, v in pairs( tItems ) do
		local str = v.Text
		if ( str:StartWith( "#" ) ) then str = str:sub( 2 ) end
		tools[ language.GetPhrase( str ) ] = v
	end

	for k, v in SortedPairs( tools ) do
		if not SMenu.frame.config.toolgun.IsAllowed(v.ItemName) then
			continue
		end

		local item = Category:Add( v.Text )

		item.DoClick = function( button )
			spawnmenu.ActiveControlPanel()

			local cpanel = controlpanel.Get(v.ItemName)
			if IsValid(cpanel) then
				timer.Simple(0.1, function() 
					self:SetActive(cpanel)
				end)
			elseif v.CPanelFunction then
				v.CPanelFunction( self.Content )
			end

			spawnmenu.SetActiveControlPanel( self.Content )
			spawnmenu.ActivateTool( v.ItemName )
			SMenu.frame.tool_selected = v.ItemName
		end

		if(SMenu.frame.tool_selected == v.ItemName) then
			spawnmenu.ActiveControlPanel()
			
			local cpanel = controlpanel.Get(v.ItemName)
			if IsValid(cpanel) then
				timer.Simple(0.1, function() 
					self:SetActive(cpanel)
				end)
			elseif v.CPanelFunction then
				v.CPanelFunction( self.Content )
			end

			spawnmenu.SetActiveControlPanel( self.Content )
			spawnmenu.ActivateTool( v.ItemName )
			item:SetSelected(true)
		end

		item.ControlPanelBuildFunction	= v.CPanelFunction
		item.Command					= v.Command
		item.Name						= v.ItemName
		item.Controls					= v.Controls
		item.Text						= v.Text

	end

	if(#Category:GetChildren() == 1) then
		Category:Remove()
	end

	self:InvalidateLayout()

end

function PANEL:SetActive( cp )

	local kids = self.Content:GetCanvas():GetChildren()
	for k, v in pairs( kids ) do
		v:SetVisible( false )
	end

	self.Content:AddItem( cp )
	cp:SetVisible( true )
	cp:Dock( FILL )

end

vgui.Register( "SToolPanel", PANEL, "Panel" )
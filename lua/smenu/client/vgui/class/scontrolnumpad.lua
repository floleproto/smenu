local PANEL = {}

AccessorFunc( PANEL, "m_ConVar1", "ConVar1" )
AccessorFunc( PANEL, "m_ConVar2", "ConVar2" )
function PANEL:Init()

	self.NumPad1 = vgui.Create( "SBinder", self )
	self.Label1 = vgui.Create( "DLabel", self )
	self.Label1:SetTextColor(SMenu.Color.GetColor("white"))
	self.Label1:SetFont("SMenu:Text")
	self.NumPad1:SetFont("SMenu:Label")

	self.NumPad2 = vgui.Create( "SBinder", self )
	self.Label2 = vgui.Create( "DLabel", self )
	self.Label2:SetTextColor(SMenu.Color.GetColor("white"))
	self.Label2:SetFont("SMenu:Text")
	self.NumPad2:SetFont("SMenu:Label")

	self:SetPaintBackground( false )

	self:SetHeight( 300 )

end
function PANEL:SetLabel1( txt )
	if ( !txt ) then return end
	self.Label1:SetText( txt )
end
function PANEL:SetLabel2( txt )
	if ( !txt ) then return end
	self.Label2:SetText( txt )
end

function PANEL:SetConVar1( cvar )
	self.NumPad1:SetConVar( cvar )
	self.m_ConVar1 = cvar
end

function PANEL:SetConVar2( cvar )
	self.NumPad2:SetConVar( cvar )
	self.m_ConVar2 = cvar
end

function PANEL:PerformLayout()

	self:SetTall( 100 )

	self.NumPad1:InvalidateLayout( true )
	self.NumPad1:SetSize( 125, 50 )

	if ( self.m_ConVar2 ) then
		self.NumPad2:InvalidateLayout( true )
		self.NumPad2:SetSize( 125, 50 )
	end

	if ( !self.m_ConVar2 ) then

		self.Label1:SizeToContents()

		self.NumPad2:SetVisible( false )
		self.Label2:SetVisible( false )

		self.NumPad1:CenterHorizontal( 0.5 )
		self.NumPad1:AlignTop( 20 )

		self.Label1:CenterHorizontal()
		self.Label1:AlignTop( 0 )

	else

		self.Label1:SizeToContents()
		self.Label2:SizeToContents()

		self.NumPad2:SetVisible( true )
		self.Label2:SetVisible( true )

		self.NumPad1:CenterHorizontal( 0.25 )
		self.Label1:CenterHorizontal( 0.25 )
		self.NumPad1:AlignTop( 20 )

		self.NumPad2:CenterHorizontal( 0.75 )
		self.Label2:CenterHorizontal( 0.75 )
		self.NumPad2:AlignTop( 20 )
		self.Label2:AlignTop( 0 )

	end

end

vgui.Register( "SCtrlNumPad", PANEL, "DPanel" )
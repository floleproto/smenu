SMenu = SMenu or {}
SMenu.frame.popup = SMenu.frame.popup or {}

function SMenu.frame.popup.Info(sentence, closeCallback)
	surface.SetFont("SMenu:Title")
	local tx, ty = surface.GetTextSize(sentence)

	local popup = vgui.Create("SFrame")
	popup:SetDrawOnTop(true)
	popup:MakePopup()
	popup:SetTitle(SMenu.Language["Information"])
	popup:SetSize(math.Clamp(tx * 1.2, 500, 1000), 300 )
	popup:Center()
	function popup:Paint(w, h)
		draw.RoundedBox(0, 0, 0, w, h, SMenu.Color.GetColor("secondary"))
		draw.RoundedBox(0, 0, 0, w, 50, SMenu.Color.GetColor("black"))
	end

	if(closeCallback != nil) then
		function popup:OnClose() 
			closeCallback()
		end
	end

	function popup:OnFocusChanged(gained) 
		popup:RequestFocus()
	end

	local textLabel = vgui.Create("SLabel", popup)
	textLabel:SetSize(tx, ty)
	textLabel:Center()
	textLabel:SetText(sentence)

	local buttonValidate = vgui.Create("SMenuButton", popup)
	buttonValidate:SetSize(200, 50)
	buttonValidate:SetPos(200, 225)
	buttonValidate:SetText(SMenu.Language["Ok"])
	buttonValidate.HoverColor = SMenu.Color.GetColor("green")
	buttonValidate:CenterHorizontal()
	function buttonValidate:DoClick()
		popup:Close()
	end

	
end
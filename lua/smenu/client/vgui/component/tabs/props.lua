SMenu.frame = SMenu.frame or {}
SMenu.frame.props = SMenu.frame.props or {}
SMenu.frameFunc = SMenu.frameFunc or {}
SMenu.frameFunc.props = SMenu.frameFunc.props or {}
SMenu.frame.props_lastSelect = ""
function SMenu.frameFunc.props.Show()

	local mpx, mpy = SMenu.frame.main_panel:GetSize()

	SMenu.frame.props.categoryIconBrowser = vgui.Create("SIconBrowser", SMenu.frame.main_panel)
	SMenu.frame.props.categoryIconBrowser:SetSize(mpx - mpx * 0.3, mpy - mpy * 0.075)
	SMenu.frame.props.categoryIconBrowser:SetPos(mpx * 0.3, mpy * 0.075)
	function SMenu.frame.props.categoryIconBrowser:Paint(w, h)
		draw.RoundedBox(0, 0, 0, w, h, SMenu.Color.GetColor("primary"))
	end

	SMenu.frame.props.categoryScrollPanel = vgui.Create("SScrollPanel", SMenu.frame.main_panel)
	SMenu.frame.props.categoryScrollPanel:SetSize(mpx * 0.3, mpy - mpy * 0.075)
	SMenu.frame.props.categoryScrollPanel:SetPos(0, mpy * 0.075)
	function SMenu.frame.props.categoryScrollPanel:Paint(w, h)
		draw.RoundedBox(0, 0, 0, w, h, SMenu.Color.GetColor("secondary"))
	end

	local csx, csy = SMenu.frame.props.categoryScrollPanel:GetSize()
	local c = 0
	for k,v in SMenu.utils.orderedPairs(SMenu.frame.config.categoryconfig.categories) do
		if(table.HasValue( v.usergroup, LocalPlayer():GetUserGroup()) or table.HasValue(v.usergroup, "*")) then
			c = c + 1
			local button = SMenu.frame.props.categoryScrollPanel:Add("SMenuButton")
			button:Dock(TOP)
			button:SetSize(csx, 75)
			button:SetText(v.name)
			button.Color = SMenu.Color.GetColor("secondary")
			if SMenu.frame.props_lastSelect == v.props || (c == 1 && SMenu.frame.props_lastSelect == "") then
				button:SetToggle(true)
				hasToggle = true
				SMenu.frameFunc.props.SetProps(v.props)
			end
			function button:DoClick()
				for _,v in pairs(SMenu.frame.props.categoryScrollPanel:GetCanvas():GetChildren()) do
					v:SetToggle(false)
				end
				button:SetToggle(true)
				SMenu.frame.props_lastSelect = v.props
				SMenu.frameFunc.props.SetProps(v.props)
			end
		end
	end
end

function SMenu.frameFunc.props.Close()
	if IsValid(SMenu.frame.props.categoryIconBrowser) then
		SMenu.frame.props.categoryIconBrowser:Remove()
	end
	if IsValid(SMenu.frame.props.categoryScrollPanel) then
		SMenu.frame.props.categoryScrollPanel:Remove()
	end
end

function SMenu.frameFunc.props.SetProps(props)
	SMenu.frame.props.categoryIconBrowser:Clear()
	for _, v in pairs(props) do
		local icon = SMenu.frame.props.categoryIconBrowser:Add("SpawnIcon")
		icon:SetModel(v)
		icon:SetSize(75, 75)
		function icon:DoClick()
			surface.PlaySound("ui/buttonclickrelease.wav")
			RunConsoleCommand("gm_spawn", v)
		end
	end
end
SMenu.frame = SMenu.frame or {}
SMenu.frame.weapons = SMenu.frame.weapons or {}
SMenu.frameFunc = SMenu.frameFunc or {}
SMenu.frameFunc.weapons = SMenu.frameFunc.weapons or {}
SMenu.frame.weapons_lastSelect = ""

function SMenu.frameFunc.weapons.Show()

	local mpx, mpy = SMenu.frame.main_panel:GetSize()

	SMenu.frame.weapons.categoryIconBrowser = vgui.Create("SIconBrowser", SMenu.frame.main_panel)
	SMenu.frame.weapons.categoryIconBrowser:SetSize(mpx - mpx * 0.3, mpy - mpy * 0.075)
	SMenu.frame.weapons.categoryIconBrowser:SetPos(mpx * 0.3, mpy * 0.075)
	function SMenu.frame.weapons.categoryIconBrowser:Paint(w, h)
		draw.RoundedBox(0, 0, 0, w, h, SMenu.Color.GetColor("primary"))
	end

	SMenu.frame.weapons.categoryScrollPanel = vgui.Create("SScrollPanel", SMenu.frame.main_panel)
	SMenu.frame.weapons.categoryScrollPanel:SetSize(mpx * 0.3, mpy - mpy * 0.075)
	SMenu.frame.weapons.categoryScrollPanel:SetPos(0, mpy * 0.075)
	function SMenu.frame.weapons.categoryScrollPanel:Paint(w, h)
		draw.RoundedBox(0, 0, 0, w, h, SMenu.Color.GetColor("secondary"))
	end

	local csx, csy = SMenu.frame.weapons.categoryScrollPanel:GetSize()
	local c = 0
	local hasToggle = false
	for k,v in SMenu.utils.orderedPairs(SMenu.entStorage.Weapons) do
		c = c + 1
		local button = SMenu.frame.weapons.categoryScrollPanel:Add("SMenuButton")
		button:Dock(TOP)
		button:SetSize(csx, 75)
		button:SetText(k)
		button.Color = SMenu.Color.GetColor("secondary")
		if SMenu.frame.weapons_lastSelect == v || (c == 1 && SMenu.frame.weapons_lastSelect == "") then
			button:SetToggle(true)
			hasToggle = true
			SMenu.frameFunc.weapons.SetWeapons(v)
		end
		function button:DoClick()
			for _,v in pairs(SMenu.frame.weapons.categoryScrollPanel:GetCanvas():GetChildren()) do
				v:SetToggle(false)
			end
			SMenu.frame.weapons_lastSelect = v
			button:SetToggle(true)
			SMenu.frameFunc.weapons.SetWeapons(v)
		end
	end
end

function SMenu.frameFunc.weapons.Close()
	if IsValid(SMenu.frame.weapons.categoryIconBrowser) then
		SMenu.frame.weapons.categoryIconBrowser:Remove()
	end

	if IsValid(SMenu.frame.weapons.categoryScrollPanel) then
		SMenu.frame.weapons.categoryScrollPanel:Remove()
	end
end

function SMenu.frameFunc.weapons.SetWeapons(weapons)
	SMenu.frame.weapons.categoryIconBrowser:Clear()
	for _, v in pairs(weapons) do
		local icon = SMenu.frame.weapons.categoryIconBrowser:Add("ContentIcon")
		icon:SetContentType( "vehicle" )
		icon:SetSpawnName( v.ClassName )
		icon:SetName( v.PrintName or v.ClassName )
		icon:SetMaterial( "entities/" .. v.ClassName .. ".png" )
		icon:SetAdminOnly( v.AdminOnly or false )
		function icon:DoClick()
			surface.PlaySound("ui/buttonclickrelease.wav")
			RunConsoleCommand( "gm_giveswep", v.ClassName )
		end

		function icon:DoMiddleClick()
			RunConsoleCommand( "gm_spawnswep", v.ClassName )
			surface.PlaySound( "ui/buttonclickrelease.wav" )
		end
	end
end
SMenu = SMenu or {}
SMenu.frame = SMenu.frame or {}
SMenu.frame.config_category = SMenu.frame.config_category or {}
SMenu.frame.config_category.cat = {}

function SMenu.frame.config_category.Show()

	SMenu.frame.config_category.panel = vgui.Create("SScrollPanel")
	local scrW, scrH = ScrW(), ScrH()

	-- CATEGORY LIST

	SMenu.frame.config_category.catCombo = vgui.Create("SComboBox", SMenu.frame.config_category.panel)
	SMenu.frame.config_category.catCombo:SetSize(scrW * 0.17, scrH * 0.05)
	SMenu.frame.config_category.catCombo:SetPos(scrW * 0.02, scrW * 0.03)

	SMenu.frame.config_category.RefreshComboBox()

	function SMenu.frame.config_category.catCombo:OnSelect(_ , _, data)
		SMenu.frame.config_category.cat = data
		SMenu.frame.config_category.RefreshValue()
	end

	SMenu.frame.config_category.addCatButton = vgui.Create("SMenuButton", SMenu.frame.config_category.panel)
	SMenu.frame.config_category.addCatButton.HoverColor = SMenu.Color.GetColor("green")
	SMenu.frame.config_category.addCatButton:SetText(SMenu.Language["Add"])
	SMenu.frame.config_category.addCatButton:SetSize(scrW * 0.05, scrH * 0.05)
	SMenu.frame.config_category.addCatButton:SetPos(scrW * 0.02 + scrW * 0.17 + 10, scrW * 0.03)

	function SMenu.frame.config_category.addCatButton.DoClick()
		SMenu.frame.config_mainframe.frame:SetMouseInputEnabled(false)
		SMenu.frame.config_mainframe.frame:SetKeyBoardInputEnabled(false)

		SMenu.frame.popup.AskText(SMenu.Language["CategoryQuestion"], function (res)
			SMenu.frame.config.categoryconfig.AddCategory(res)
			SMenu.frame.config_category.RefreshComboBox()

			SMenu.frame.config_mainframe.frame:SetMouseInputEnabled(true)
			SMenu.frame.config_mainframe.frame:SetKeyBoardInputEnabled(true)
		end, function ()
			SMenu.frame.config_mainframe.frame:SetMouseInputEnabled(true)
			SMenu.frame.config_mainframe.frame:SetKeyBoardInputEnabled(true)
		end)
	end

	SMenu.frame.config_category.removeCatButton = vgui.Create("SMenuButton", SMenu.frame.config_category.panel)
	SMenu.frame.config_category.removeCatButton.HoverColor = SMenu.Color.GetColor("red")
	SMenu.frame.config_category.removeCatButton:SetText(SMenu.Language["Remove"])
	SMenu.frame.config_category.removeCatButton:SetSize(scrW * 0.05, scrH * 0.05)
	SMenu.frame.config_category.removeCatButton:SetPos(scrW * 0.02 + scrW * 0.05 + scrW * 0.17 + 20, scrW * 0.03)

	function SMenu.frame.config_category.removeCatButton.DoClick()
		SMenu.frame.config_mainframe.frame:SetMouseInputEnabled(false)
		SMenu.frame.config_mainframe.frame:SetKeyBoardInputEnabled(false)

		SMenu.frame.popup.Confirm(SMenu.Language["ConfirmDelete"], function ()
			SMenu.frame.config.categoryconfig.RemoveCategory(SMenu.frame.config_category.cat["name"])
			SMenu.frame.config_category.RefreshComboBox()

			SMenu.frame.config_mainframe.frame:SetMouseInputEnabled(true)
			SMenu.frame.config_mainframe.frame:SetKeyBoardInputEnabled(true)
		end, function ()
			SMenu.frame.config_mainframe.frame:SetMouseInputEnabled(true)
			SMenu.frame.config_mainframe.frame:SetKeyBoardInputEnabled(true)
		end)
	end

	SMenu.frame.config_category.catLabel = vgui.Create("SLabel", SMenu.frame.config_category.panel)
	SMenu.frame.config_category.catLabel:SetText(SMenu.Language["Category"])
	SMenu.frame.config_category.catLabel:SetSize(scrW * 0.05, scrH * 0.05)
	SMenu.frame.config_category.catLabel:SetPos(scrW * 0.02, scrW * 0.002)

	-- PROPS

	SMenu.frame.config_category.propsLabel = vgui.Create("SLabel", SMenu.frame.config_category.panel)
	SMenu.frame.config_category.propsLabel:SetText(SMenu.Language["Props"])
	SMenu.frame.config_category.propsLabel:SetSize(scrW * 0.05, scrH * 0.05)
	SMenu.frame.config_category.propsLabel:SetPos(scrW * 0.02, scrW * 0.06)

	SMenu.frame.config_category.propsNumber = vgui.Create("SLabel", SMenu.frame.config_category.panel)
	SMenu.frame.config_category.propsNumber:SetSize(scrW * 0.3, scrH * 0.05)
	SMenu.frame.config_category.propsNumber:SetPos(scrW * 0.02, scrW * 0.072)
	SMenu.frame.config_category.propsNumber:SetFont("SMenu:Text")

	SMenu.frame.config_category.openPropsButton = vgui.Create("SMenuButton", SMenu.frame.config_category.panel)
	SMenu.frame.config_category.openPropsButton:SetText(SMenu.Language["EditProps"])
	SMenu.frame.config_category.openPropsButton:SetSize(scrW * 0.28, scrH * 0.05)
	SMenu.frame.config_category.openPropsButton:SetPos(scrW * 0.02, scrW * 0.095)

	function SMenu.frame.config_category.openPropsButton.DoClick()
		SMenu.frame.config_mainframe.frame:SetMouseInputEnabled(false)
		SMenu.frame.config_mainframe.frame:SetKeyBoardInputEnabled(false)

		SMenu.frame.config_category.showProps(function ()
			SMenu.frame.config_mainframe.frame:SetMouseInputEnabled(true)
			SMenu.frame.config_mainframe.frame:SetKeyBoardInputEnabled(true)
		end)
	end

	-- User Groups

	SMenu.frame.config_category.userGroupsLabel = vgui.Create("SLabel", SMenu.frame.config_category.panel)
	SMenu.frame.config_category.userGroupsLabel:SetText(SMenu.Language["UserGroup"])
	SMenu.frame.config_category.userGroupsLabel:SetSize(scrW * 0.3, scrH * 0.05)
	SMenu.frame.config_category.userGroupsLabel:SetPos(scrW * 0.02, scrW * 0.12)

	SMenu.frame.config_category.userNumber = vgui.Create("SLabel", SMenu.frame.config_category.panel)
	SMenu.frame.config_category.userNumber:SetSize(scrW * 0.3, scrH * 0.05)
	SMenu.frame.config_category.userNumber:SetPos(scrW * 0.02, scrW * 0.13)
	SMenu.frame.config_category.userNumber:SetFont("SMenu:Text")

	SMenu.frame.config_category.openUserButton = vgui.Create("SMenuButton", SMenu.frame.config_category.panel)
	SMenu.frame.config_category.openUserButton:SetText(SMenu.Language["EditUserGroups"])
	SMenu.frame.config_category.openUserButton:SetSize(scrW * 0.28, scrH * 0.05)
	SMenu.frame.config_category.openUserButton:SetPos(scrW * 0.02, scrW * 0.153)

	function SMenu.frame.config_category.openUserButton.DoClick()
		SMenu.frame.config_mainframe.frame:SetMouseInputEnabled(false)
		SMenu.frame.config_mainframe.frame:SetKeyBoardInputEnabled(false)

		SMenu.frame.config_category.showUsergroup(function ()
			SMenu.frame.config_mainframe.frame:SetMouseInputEnabled(true)
			SMenu.frame.config_mainframe.frame:SetKeyBoardInputEnabled(true)
		end)
	end

	SMenu.frame.config_category.RefreshValue()
	return SMenu.frame.config_category.panel

end

function SMenu.frame.config_category.RefreshComboBox()
	if IsValid(SMenu.frame.config_category.panel) then
		SMenu.frame.config_category.catCombo:Clear()
		for _,v in pairs(SMenu.frame.config.categoryconfig.categories) do
			SMenu.frame.config_category.catCombo:AddChoice(v["name"], v)
		end
	
		if table.Count(SMenu.frame.config.categoryconfig.categories) > 0 then
			SMenu.frame.config_category.catCombo:ChooseOptionID(1)
			SMenu.frame.config_category.cat = SMenu.frame.config.categoryconfig.categories[1]
		end
	end
end

function SMenu.frame.config_category.RefreshValue()
	if IsValid(SMenu.frame.config_category.panel) then
		SMenu.frame.config_category.propsNumber:SetText(string.format(SMenu.Language["NumberProps"], table.Count(SMenu.frame.config_category.cat["props"])))
		SMenu.frame.config_category.userNumber:SetText(string.format(SMenu.Language["NumberUserGroupCategory"], table.Count(SMenu.frame.config_category.cat["usergroup"])))
	end
end

function SMenu.frame.config_category.Close()
	if IsValid(SMenu.frame.config_category.panel) then
		SMenu.frame.config_category.panel:Remove()
	end
end

function SMenu.frame.config_category.IsOpen()
	return IsValid(SMenu.frame.config_category.panel)
end
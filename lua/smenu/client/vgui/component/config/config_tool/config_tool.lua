SMenu = SMenu or {}
SMenu.frame = SMenu.frame or {}
SMenu.frame.config_tool = SMenu.frame.config_tool or {}
SMenu.frame.config_tool.tool = {}
SMenu.frame.config_tool.toolname = ""

function SMenu.frame.config_tool.Show()

	SMenu.frame.config_tool.panel = vgui.Create("SScrollPanel")
	local scrW, scrH = ScrW(), ScrH()

	-- TABS LIST

	SMenu.frame.config_tool.catCombo = vgui.Create("SComboBox", SMenu.frame.config_tool.panel)
	SMenu.frame.config_tool.catCombo:SetSize(scrW * 0.17, scrH * 0.05)
	SMenu.frame.config_tool.catCombo:SetPos(scrW * 0.02, scrW * 0.03)

	SMenu.frame.config_tool.RefreshComboBox()

	function SMenu.frame.config_tool.catCombo:OnSelect(_ , value, data)
		SMenu.frame.config_tool.tool = data
		SMenu.frame.config_tool.RefreshValue()
		SMenu.frame.config_tool.toolname = value
	end
	SMenu.frame.config_tool.catLabel = vgui.Create("SLabel", SMenu.frame.config_tool.panel)
	SMenu.frame.config_tool.catLabel:SetText(SMenu.Language["Tools"])
	SMenu.frame.config_tool.catLabel:SetSize(scrW * 0.05, scrH * 0.05)
	SMenu.frame.config_tool.catLabel:SetPos(scrW * 0.02, scrW * 0.002)

	SMenu.frame.config_tool.addCatButton = vgui.Create("SMenuButton", SMenu.frame.config_tool.panel)
	SMenu.frame.config_tool.addCatButton.HoverColor = SMenu.Color.GetColor("green")
	SMenu.frame.config_tool.addCatButton:SetText(SMenu.Language["Add"])
	SMenu.frame.config_tool.addCatButton:SetSize(scrW * 0.05, scrH * 0.05)
	SMenu.frame.config_tool.addCatButton:SetPos(scrW * 0.02 + scrW * 0.17 + 10, scrW * 0.03)

	function SMenu.frame.config_tool.addCatButton.DoClick()
		SMenu.frame.config_mainframe.frame:SetMouseInputEnabled(false)
		SMenu.frame.config_mainframe.frame:SetKeyBoardInputEnabled(false)

		SMenu.frame.popup.ToolgunPopup(function (res)
			SMenu.frame.config.toolgun.AddToolgun(res)
			SMenu.frame.config_tool.RefreshComboBox()
			SMenu.frame.config_mainframe.frame:SetMouseInputEnabled(true)
			SMenu.frame.config_mainframe.frame:SetKeyBoardInputEnabled(true)
		end, SMenu.frame.config_tool.tool, function ()
			SMenu.frame.config_mainframe.frame:SetMouseInputEnabled(true)
			SMenu.frame.config_mainframe.frame:SetKeyBoardInputEnabled(true)
		end)
	end

	SMenu.frame.config_tool.removeCatButton = vgui.Create("SMenuButton", SMenu.frame.config_tool.panel)
	SMenu.frame.config_tool.removeCatButton.HoverColor = SMenu.Color.GetColor("red")
	SMenu.frame.config_tool.removeCatButton:SetText(SMenu.Language["Remove"])
	SMenu.frame.config_tool.removeCatButton:SetSize(scrW * 0.05, scrH * 0.05)
	SMenu.frame.config_tool.removeCatButton:SetPos(scrW * 0.02 + scrW * 0.05 + scrW * 0.17 + 20, scrW * 0.03)

	function SMenu.frame.config_tool.removeCatButton.DoClick()
		SMenu.frame.config_mainframe.frame:SetMouseInputEnabled(false)
		SMenu.frame.config_mainframe.frame:SetKeyBoardInputEnabled(false)

		SMenu.frame.popup.Confirm(SMenu.Language["ConfirmDelete"], function ()
			SMenu.frame.config.toolgun.RemoveToolgun(SMenu.frame.config_tool.toolname)
			SMenu.frame.config_tool.RefreshComboBox()
			SMenu.frame.config_mainframe.frame:SetMouseInputEnabled(true)
			SMenu.frame.config_mainframe.frame:SetKeyBoardInputEnabled(true)
		end, function ()
			SMenu.frame.config_mainframe.frame:SetMouseInputEnabled(true)
			SMenu.frame.config_mainframe.frame:SetKeyBoardInputEnabled(true)
		end)
	end

	-- USER

	SMenu.frame.config_tool.userLabel = vgui.Create("SLabel", SMenu.frame.config_tool.panel)
	SMenu.frame.config_tool.userLabel:SetText(SMenu.Language["UserGroup"])
	SMenu.frame.config_tool.userLabel:SetSize(scrW * 0.2, scrH * 0.05)
	SMenu.frame.config_tool.userLabel:SetPos(scrW * 0.02, scrW * 0.06)

	SMenu.frame.config_tool.userNumber = vgui.Create("SLabel", SMenu.frame.config_tool.panel)
	SMenu.frame.config_tool.userNumber:SetSize(scrW * 0.3, scrH * 0.05)
	SMenu.frame.config_tool.userNumber:SetPos(scrW * 0.02, scrW * 0.072)
	SMenu.frame.config_tool.userNumber:SetFont("SMenu:Text")

	SMenu.frame.config_tool.openUserMenu = vgui.Create("SMenuButton", SMenu.frame.config_tool.panel)
	SMenu.frame.config_tool.openUserMenu:SetText(SMenu.Language["EditUserGroups"])
	SMenu.frame.config_tool.openUserMenu:SetSize(scrW * 0.28, scrH * 0.05)
	SMenu.frame.config_tool.openUserMenu:SetPos(scrW * 0.02, scrW * 0.095)

	function SMenu.frame.config_tool.openUserMenu.DoClick()
		SMenu.frame.config_mainframe.frame:SetMouseInputEnabled(false)
		SMenu.frame.config_mainframe.frame:SetKeyBoardInputEnabled(false)

		SMenu.frame.config_tool.showUsergroup(function ()
			SMenu.frame.config_mainframe.frame:SetMouseInputEnabled(true)
			SMenu.frame.config_mainframe.frame:SetKeyBoardInputEnabled(true)
		end)
	end

	SMenu.frame.config_tool.RefreshValue()
	return SMenu.frame.config_tool.panel

end

function SMenu.frame.config_tool.RefreshComboBox()
	if IsValid(SMenu.frame.config_tool.panel) then
		SMenu.frame.config_tool.catCombo:Clear()
		for k, v in pairs(SMenu.frame.config.toolgun.toolgun) do
			SMenu.frame.config_tool.catCombo:AddChoice(k, v)
		end
	
		if table.Count(SMenu.frame.config.categoryconfig.categories) > 0 then
			SMenu.frame.config_tool.catCombo:ChooseOptionID(1)
			SMenu.frame.config_tool.tool = table.GetFirstValue(SMenu.frame.config.toolgun.toolgun) 
			SMenu.frame.config_tool.toolname = table.GetFirstKey(SMenu.frame.config.toolgun.toolgun) 
		end
	end
end

function SMenu.frame.config_tool.RefreshValue()
	if IsValid(SMenu.frame.config_tool.panel) then
		SMenu.frame.config_tool.userNumber:SetText(string.format(SMenu.Language["NumberUserTool"], table.Count(SMenu.frame.config_tool.tool)))
	end
end

function SMenu.frame.config_tool.Close()
	if IsValid(SMenu.frame.config_tool.panel) then
		SMenu.frame.config_tool.panel:Remove()
	end
end

function SMenu.frame.config_tool.IsOpen()
	return IsValid(SMenu.frame.config_tool.panel)
end
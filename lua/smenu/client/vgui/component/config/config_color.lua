SMenu = SMenu or {}
SMenu.frame = SMenu.frame or {}
SMenu.frame.config_color = SMenu.frame.config_color or {}
SMenu.frame.config_color.panel = {}

function SMenu.frame.config_color.Show()

	SMenu.frame.config_color.panel = vgui.Create("SScrollPanel")
	
	local scrH, scrW = ScrH(), ScrW()
	
	SMenu.frame.config_color.colorLabel = vgui.Create("DLabel", SMenu.frame.config_color.panel)
	SMenu.frame.config_color.colorLabel:SetSize(scrW * 0.125, scrH * 0.05)
	SMenu.frame.config_color.colorLabel:SetPos(scrW * 0.02, scrW * 0.002)
	SMenu.frame.config_color.colorLabel:SetText(SMenu.Language["Accentuation"])
	SMenu.frame.config_color.colorLabel:SetFont("SMenu:Title")
	SMenu.frame.config_color.colorLabel:SetTextColor(SMenu.Color.GetColor("white"))
	
	SMenu.frame.config_color.colorpanel = vgui.Create("SColorMixer", SMenu.frame.config_color.panel)
	SMenu.frame.config_color.colorpanel:SetAlphaBar(false)
	SMenu.frame.config_color.colorpanel:SetColor(SMenu.Color.GetAccentColor())
	SMenu.frame.config_color.colorpanel:SetSize(scrW * 0.28, scrH * 0.2)
	SMenu.frame.config_color.colorpanel:SetPos(scrW * 0.02, scrW * 0.03)
	
	SMenu.frame.config_color.preview1 = vgui.Create("DPanel",  SMenu.frame.config_color.panel)
	SMenu.frame.config_color.preview1:SetSize(scrW * 0.125, scrH * 0.05)
	SMenu.frame.config_color.preview1:SetPos(scrW * 0.02, scrW * 0.15)

	function SMenu.frame.config_color.preview1:Paint(w, h)
		local color =  SMenu.Color.AdditionColor(SMenu.frame.config_color.colorpanel:GetColor(), SMenu.Color["accent"])
		draw.RoundedBox(0, 0, 0, w, h, color)
	end

	SMenu.frame.config_color.preview2 = vgui.Create("DPanel", SMenu.frame.config_color.panel) 
	SMenu.frame.config_color.preview2:SetSize(scrW * 0.125, scrH * 0.05)
	SMenu.frame.config_color.preview2:SetPos((scrW * 0.1 + scrW * 0.02 + 10 ) * 1.4, scrW * 0.15)

	function SMenu.frame.config_color.preview2:Paint(w, h)
		local color =  SMenu.Color.AdditionColor(SMenu.frame.config_color.colorpanel:GetColor(), SMenu.Color["accentdark"])
		draw.RoundedBox(0, 0, 0, w, h, color)
	end

	SMenu.frame.config_color.themeLabel = vgui.Create("DLabel", SMenu.frame.config_color.panel)
	SMenu.frame.config_color.themeLabel:SetSize(scrW * 0.05, scrH * 0.65)
	SMenu.frame.config_color.themeLabel:SetPos(scrW * 0.02, scrW * 0.01)
	SMenu.frame.config_color.themeLabel:SetText(SMenu.Language["Theme"])
	SMenu.frame.config_color.themeLabel:SetFont("SMenu:Title")
	SMenu.frame.config_color.themeLabel:SetTextColor(SMenu.Color.GetColor("white"))

	SMenu.frame.config_color.theme = vgui.Create("SComboBox", SMenu.frame.config_color.panel) 
	SMenu.frame.config_color.theme:SetSize(scrW * 0.28, scrH * 0.05)
	SMenu.frame.config_color.theme:SetPos(scrW * 0.02, scrW * 0.205)
	SMenu.frame.config_color.theme:SetFont("SMenu:Text")

	SMenu.frame.config_color.theme:AddChoice(SMenu.Language["Dark"], "dark", true)
	SMenu.frame.config_color.theme:AddChoice(SMenu.Language["Light"], "light", SMenu.Color.IsLightTheme())

	SMenu.frame.config_color.buttonsave = vgui.Create("SMenuButton", SMenu.frame.config_color.panel)
	SMenu.frame.config_color.buttonsave.HoverColor = SMenu.Color.GetColor("green")
	SMenu.frame.config_color.buttonsave:SetSize(scrW * 0.28, scrH * 0.05)
	SMenu.frame.config_color.buttonsave:SetPos(scrW * 0.02, scrW * 0.25)
	SMenu.frame.config_color.buttonsave:SetText(SMenu.Language["Save"])

	function SMenu.frame.config_color.buttonsave.DoClick()
		local color = SMenu.frame.config_color.colorpanel:GetColor()
		local _, str = SMenu.frame.config_color.theme:GetSelected()
		SMenu.Color.SetAccentColor(color.r, color.g, color.b)
		SMenu.Color.SetIsLightTheme(str == "light")
		SMenu.Color.SendToGlobalConfig()
		SMenu.Color.LoadColors()

		SMenu.frame.popup.Info(SMenu.Language["SuccessSave"], function()
		end)
		SMenu.frame.config_mainframe.frame:Close()
	end

	return SMenu.frame.config_color.panel

end

function SMenu.frame.config_color.Close()
	if IsValid(SMenu.frame.config_color.panel) then
		SMenu.frame.config_color.panel:Remove()
	end
end

function SMenu.frame.config_color.IsOpen()
	return IsValid(SMenu.frame.config_color.panel)
end
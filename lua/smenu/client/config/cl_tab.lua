SMenu = SMenu or {}
SMenu.frame = SMenu.frame or {}
SMenu.frame.config = SMenu.frame.config or {}
SMenu.frame.config.tabs = SMenu.frame.config.tabs or {}
SMenu.frame.config.tabs.tabs = {}

function SMenu.frame.config.tabs.AddTab(name)
	SMenu.frame.config.tabs.tabs[name] = {"*"}
	SMenu.frame.config.tabs.SendToGlobalConfig()
end

function SMenu.frame.config.tabs.ModifyTab(name, value)
	SMenu.frame.config.tabs.tabs[name] = value
end

function SMenu.frame.config.tabs.Reload()
	SMenu.frame.config.tabs.tabs = SMenu.ConfigManager.GetConfig("tabs")
end

function SMenu.frame.config.tabs.SendToGlobalConfig()
	SMenu.ConfigManager.ModifyConfig("tabs", SMenu.frame.config.tabs.tabs)
end

function SMenu.frame.config.tabs.RemoveTab(name)
	table.remove(SMenu.frame.config.tabs.tabs, name)
	SMenu.frame.config.tabs.SendToGlobalConfig()
end

function SMenu.frame.config.tabs.isAllowedToSeeTab(ply, name)
	return table.HasValue(SMenu.frame.config.tabs.tabs[name], ply:GetUserGroup()) or table.HasValue(SMenu.frame.config.tabs.tabs[name], "*")
end
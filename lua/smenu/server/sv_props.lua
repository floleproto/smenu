
SMenu = SMenu or {}
SMenu.Props = SMenu.Props or {}

local PL = FindMetaTable("Player")

function PL:CanSpawnProps(mdl)
	if table.HasValue(SMenu.Config.IgnorePropVerification, self:GetUserGroup()) then
		return true
	end

	local cat = SMenu.ConfigManager.GetConfig("categories")
	for k,v in pairs(cat.value) do
		if table.HasValue(v.usergroup, self:GetUserGroup()) or table.HasValue(v.usergroup, "*") and table.HasValue(v.props, mdl) then
			return true
		end
	end
	self:SendNotification(SMenu.Language["PropsNotAllowed"], 1)
	return false
end

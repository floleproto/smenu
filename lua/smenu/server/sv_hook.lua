SMenu = SMenu or {}

hook.Add("PlayerInitialSpawn", "SMenu:PlayerInitialSpawn:ReloadConfig", function(ply)
	SMenu.ConfigManager.ForceReloadPlayer(ply)
end)

hook.Add("PlayerSpawnProp", "SMenu:PlayerSpawnProp:CheckCanBeSpawned", function (ply, model, entity)

	return ply:CanSpawnProps(model) && SMenu.Config.PropVerification
	
end)

hook.Add("CanTool", "SMenu:CanTool:CheckCanBeUsed", function(ply, tr, tool)

	if(ply:IsAllowed(tool) || !SMenu.Config.ToolVerification) then
		return true
	end

	ply:SendNotification(SMenu.Language["PropsNotAllowed"], 1)
	return false
end)
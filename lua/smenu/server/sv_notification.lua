local PL = FindMetaTable("Player")

function PL:SendNotification(text, type)
	
	net.Start("SMenu:Notification")
	net.WriteString(text)
	net.WriteInt(type, 4)
	net.Send(self)

end